# DailySelfie #

* This is a final task from the Android courses "Programming Mobile Applications for Android Handheld Systems: Part 2" [Coursera.com]

### This app allows you ###

* make new photo 
* watch list of maked photos
* delete photo(photos)
* set alarm to remind yourself to create new photo.

### Used APIs ###

* Loader
* Alarm manager
* Context Action Bar

# Screenshots #
![1](https://bytebucket.org/vkuzub/dailyselfie/raw/7b461910c717df8cbad7065f854cebcbbb30577b/Screens/1.png)
![2](https://bytebucket.org/vkuzub/dailyselfie/raw/7b461910c717df8cbad7065f854cebcbbb30577b/Screens/2.png)
![3](https://bytebucket.org/vkuzub/dailyselfie/raw/7b461910c717df8cbad7065f854cebcbbb30577b/Screens/3.png)