package com.vkuzub.dailyselfie;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;

import java.util.concurrent.TimeUnit;

/**
 * Created by Vyacheslav on 24.01.2016.
 */
public class AlarmReceiver extends BroadcastReceiver {

    public static final int NOTIFY_ID = 101;
    public static final int MINUTE = 60;

    @Override
    public void onReceive(Context context, Intent intent) {
        showNotification(context);
    }

    private void showNotification(Context context) {
        System.out.println(context.getString(R.string.time_to_make_selfie));
        NotificationManager notificationManager = (NotificationManager) context.
                getSystemService(Context.NOTIFICATION_SERVICE);
        Intent intent = new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent,
                PendingIntent.FLAG_CANCEL_CURRENT);
        Notification notification = createNotification(context, pendingIntent);
        notificationManager.notify(NOTIFY_ID, notification);
    }

    private Notification createNotification(Context context, PendingIntent pendingIntent) {
        String timePrefs = PreferenceManager.getDefaultSharedPreferences(context)
                .getString(context.getString(R.string.prefs_repeating_time),
                        context.getString(R.string.entry_def_value));
        int minutes = Integer.parseInt(timePrefs) / MINUTE;
        String timePassed = String.format(context.getString(R.string.x_minute_passed_since_last_photo),
                minutes);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setContentTitle(context.getString(R.string.app_name)).
                setContentText(timePassed).
                setContentIntent(pendingIntent).
                setWhen(System.currentTimeMillis()).
                setAutoCancel(true).
                setTicker(context.getString(R.string.time_to_make_selfie)).
                setSmallIcon(R.drawable.placeholder).
                setDefaults(Notification.DEFAULT_ALL);
        return builder.build();
    }
}
