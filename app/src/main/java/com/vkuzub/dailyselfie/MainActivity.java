package com.vkuzub.dailyselfie;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.github.androidprogresslayout.ProgressLayout;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>,
        MultiChoiceItemImpl.OnDeleteItemsClickListener {

    public static final String LOG_TAG = "DailySelfie";

    public static final int REQUEST_IMAGE_CAPTURE = 1;
    public static final int PICTURES_QUERY_LOADER = 1;
    public static final String SORT_ORDER_PARAM = "sort_order";

    private String mCurrentPhotoPath;
    private String mSortOrder = "ASC";
    private SimpleCursorAdapter mAdapter;

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.lvContainer)
    ListView lvContainer;

    @Bind(R.id.progressLayout)
    ProgressLayout progressLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        initToolbar();

        initList();
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
    }

    private void initList() {
        progressLayout.showProgress();

        String[] from = {MediaStore.Images.ImageColumns.TITLE, MediaStore.Images.ImageColumns.DATA};
        int[] to = {R.id.tvItemTitle, R.id.ivItemImage};

        mAdapter = new SimpleCursorAdapter(this, R.layout.item_photo, null, from, to, 0) {

            @Override
            public View newView(Context context, Cursor cursor, ViewGroup parent) {
                int resourceId = R.layout.item_photo;
                LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
                return layoutInflater.inflate(resourceId, parent, false);
            }

            @Override
            public void bindView(View view, Context context, Cursor cursor) {

                TextView textView = (TextView) view.findViewById(R.id.tvItemTitle);
                ImageView imageView = (ImageView) view.findViewById(R.id.ivItemImage);

                int titleIndex = cursor.getColumnIndex(MediaStore.Images.ImageColumns.TITLE);
                int dataIndex = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);

                String title = cursor.getString(titleIndex);
                String dataPath = cursor.getString(dataIndex);

                Uri path = Uri.fromFile(new File(dataPath));

                textView.setText(title);
                Glide.with(context)
                        .load(path)
                        .override(160, 120)
                        .into(imageView);


            }
        };

        lvContainer.setAdapter(mAdapter);

        lvContainer.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE_MODAL);
        lvContainer.setMultiChoiceModeListener(new MultiChoiceItemImpl(lvContainer, this));

        Bundle bundle = new Bundle();
        bundle.putString(SORT_ORDER_PARAM, mSortOrder);
        getSupportLoaderManager().initLoader(PICTURES_QUERY_LOADER, bundle, this);
    }

    @OnItemClick(R.id.lvContainer)
    void onListItemClick(AdapterView<?> adapterView, int position) {
        Cursor cursor = (Cursor) adapterView.getAdapter().getItem(position);
        int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        String path = cursor.getString(index);
        openImage(path);
    }

    void openImage(String imagePath) {
        Intent openImageIntent = new Intent(Intent.ACTION_VIEW);

        if (openImageIntent.resolveActivity(getPackageManager()) != null) {
            openImageIntent.setDataAndType(Uri.parse("file://" + imagePath), "image/*");
            startActivity(openImageIntent);
        } else {
            Toast.makeText(this, R.string.cant_open_an_image, Toast.LENGTH_SHORT).show();
        }
    }

    boolean removeImage(String imagePath) {
        File image = new File(imagePath);
        if (image.isFile()) {
            if (image.exists()) {
                if (image.delete()) {
                    mCurrentPhotoPath = imagePath;
                    removeImageFromProvider(mCurrentPhotoPath);
//                    updateProviderContent();
                    Toast.makeText(this, R.string.image_deleted, Toast.LENGTH_SHORT).show();
                    return true;
                }
            }
        }
        Toast.makeText(this, R.string.cant_delete_file, Toast.LENGTH_SHORT).show();
        return false;
    }

    //slow solution
    private void removeImageFromProvider(String imagePath) {
        ContentResolver contentResolver = getContentResolver();
        contentResolver.delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                MediaStore.Images.ImageColumns.DATA + "=?", new String[]{imagePath});

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            showPreferences();
            return true;
        }
        if (id == R.id.action_sort) {
            sortOrderSwap();
            updatePictureList();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showPreferences() {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    private void sortOrderSwap() {
        if (mSortOrder.equals("ASC")) {
            mSortOrder = "DESC";
        } else {
            mSortOrder = "ASC";
        }
    }

    @OnClick(R.id.fabMakeSelfie)
    void makePhoto() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {

            File photoFile = null;

            try {
                photoFile = createImageFile();
            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(this, R.string.cant_create_photo, Toast.LENGTH_SHORT).show();
                return;
            }

            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                System.out.println(Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }

        } else {
            Toast.makeText(this, R.string.no_app_to_take_photo, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            updateProviderContent();
            updatePictureList();
            addNewAlarm();
        }
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getAlbumDir();
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private File getAlbumDir() {
        File storageDir = null;

        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), getAlbumName());
            if (storageDir != null) {
                if (!storageDir.mkdir()) {
                    if (!storageDir.exists()) {
                        Log.e(LOG_TAG, getString(R.string.failed_to_create_photo));
                        Toast.makeText(this, R.string.cant_create_photo, Toast.LENGTH_SHORT).show();
                        return null;
                    }
                }
            }
        } else {
            Log.e(LOG_TAG, getString(R.string.external_storage_not_mounted_rw));
            Toast.makeText(this, R.string.cant_create_photo, Toast.LENGTH_SHORT).show();
        }
        return storageDir;
    }

    private void updateProviderContent() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File file = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(file);
        mediaScanIntent.setData(contentUri);
        sendBroadcast(mediaScanIntent);
    }

    private void updatePictureList() {
        Bundle bundle = new Bundle();
        bundle.putString(SORT_ORDER_PARAM, mSortOrder);
        getSupportLoaderManager().restartLoader(REQUEST_IMAGE_CAPTURE, bundle, this);
    }

    private String getAlbumName() {
        return getString(R.string.app_name);
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Loader<Cursor> loader = null;

        switch (id) {
            case PICTURES_QUERY_LOADER:
                String[] projection = {
                        MediaStore.Images.ImageColumns._ID,
                        MediaStore.Images.ImageColumns.DATA,
                        MediaStore.Images.ImageColumns.TITLE,
                        MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME};

                String selection = "(" + MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME + " = ?)";
                String[] selectionArgs = {getAlbumName()};

                String orderValue = args.getString(SORT_ORDER_PARAM);
                String sortOrder = MediaStore.Images.ImageColumns.TITLE + " " + orderValue;

                loader = new CursorLoader(this, MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        projection, selection, selectionArgs, sortOrder);
                break;
        }

        return loader;
    }

    @Override
    public void onLoadFinished(Loader loader, Cursor cursor) {
        switch (loader.getId()) {
            case PICTURES_QUERY_LOADER:
                if (cursor.getCount() > 0) {
                    mAdapter.swapCursor(cursor);
                    progressLayout.showContent();
                } else {
                    progressLayout.showErrorText(getString(R.string.no_selfies));
                }
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader loader) {
        switch (loader.getId()) {
            case PICTURES_QUERY_LOADER:
                mAdapter.swapCursor(null);
                break;
        }
    }

    @Override
    public void onDeleteItemClick(List<String> items) {
        for (String pic : items) {
            removeImage(pic);
        }
    }

    private void addNewAlarm() {
        Intent intent = new Intent(this, AlarmReceiver.class);
        PendingIntent sender = PendingIntent.getBroadcast(this, 0, intent, 0);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        String timePrefs = PreferenceManager.getDefaultSharedPreferences(this)
                .getString(getString(R.string.prefs_repeating_time), getString(R.string.entry_def_value));
        if (!TextUtils.isEmpty(timePrefs)) {
            int time = Integer.parseInt(timePrefs);
            if (time != 0) {
                calendar.add(Calendar.SECOND, time);

                AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
                am.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), sender);
                Log.i(LOG_TAG, getString(R.string.alarm_started));
            }
        }

    }
}
