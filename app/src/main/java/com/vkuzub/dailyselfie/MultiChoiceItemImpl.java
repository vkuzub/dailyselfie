package com.vkuzub.dailyselfie;

import android.database.Cursor;
import android.provider.MediaStore;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.AbsListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vyacheslav on 23.01.2016.
 */
public class MultiChoiceItemImpl implements AbsListView.MultiChoiceModeListener {

    private AbsListView listView;
    private OnDeleteItemsClickListener listener;

    public MultiChoiceItemImpl(AbsListView listView, OnDeleteItemsClickListener listener) {
        this.listView = listView;
        this.listener = listener;
    }

    @Override
    public void onItemCheckedStateChanged(ActionMode actionMode, int i, long l, boolean b) {
        int selected = listView.getCheckedItemCount();
        actionMode.setSubtitle(String.valueOf(selected));
    }

    @Override
    public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
        MenuInflater inflater = actionMode.getMenuInflater();
        inflater.inflate(R.menu.menu_context, menu);
        actionMode.setTitle(R.string.select_image_for_removing);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
        if (menuItem.getItemId() == R.id.menuRemove) {
            List<String> items = getSelectedFiles();
            listener.onDeleteItemClick(items);
            actionMode.finish();
        }
        return true;
    }

    @Override
    public void onDestroyActionMode(ActionMode actionMode) {
    }

    private List<String> getSelectedFiles() {
        List<String> selectedFiles = new ArrayList<>();

        SparseBooleanArray sparseBooleanArray = listView.getCheckedItemPositions();
        for (int i = 0; i < sparseBooleanArray.size(); i++) {
            if (sparseBooleanArray.valueAt(i)) {
                Cursor cursor = (Cursor) listView.getItemAtPosition(i);
                int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                String path = cursor.getString(index);
                selectedFiles.add(path);
            }
        }
        return selectedFiles;
    }

    public interface OnDeleteItemsClickListener {
        void onDeleteItemClick(List<String> items);
    }

}
